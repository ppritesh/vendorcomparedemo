
export const criteriaActions = {
    insert,
    update,
    deleteItem
}

function insert(data){
    return {
        type: 'INSERT_CRITERIA_ITEM',
        payload: data
    }
}

function update(index,data){
    return {
        type: 'UPDATE_CRITERIA_ITEM',
        index,
        payload: data
    }
}

function deleteItem(index){
    return {
        type: 'DELETE_CRITERIA_ITEM',
        payload: index
    }
}