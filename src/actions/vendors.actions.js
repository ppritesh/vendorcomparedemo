
export const vendorActions = {
    insert,
    update,
    deleteItem
}

function insert(data){
    return {
        type: 'INSERT_VENDOR',
        payload: data
    }
}

function update(data){
    return {
        type: 'UPDATE_VENDOR',
        payload: data
    }
}

function deleteItem(index){
    return {
        type: 'DELETE_VENDOR',
        payload: index
    }
}