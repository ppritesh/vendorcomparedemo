
export function criteriaList(state = {}, action) {
    var list = JSON.parse(localStorage.getItem('criteriaList'))
 
    switch (action.type) {
        case  "INSERT_CRITERIA_ITEM" :
            list.push(action.payload);
            localStorage.setItem('criteriaList', JSON.stringify(list));
            return {list, currentIndex :state.currentIndex};
            
        case  "UPDATE_CRITERIA_ITEM" :
            list[action.index] = action.payload;
            localStorage.setItem('criteriaList', JSON.stringify(list));
            return {list, currentIndex : state.currentIndex};
        
                  
        case  "DELETE_CRITERIA_ITEM" :
            list.splice(action.payload, 1);
            localStorage.setItem('criteriaList', JSON.stringify(list));
            return {list, currentIndex : -1};
           
        default:
            return state;
    }
} 