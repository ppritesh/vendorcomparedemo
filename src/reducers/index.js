
import {criteriaList} from './criteria.reducer';
import {vendorList} from './vendors.reducer';
export const reducers ={
    criteriaList,
    vendorList
} ;