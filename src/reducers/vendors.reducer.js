


export function vendorList(state = {}, action) {
    var list = JSON.parse(localStorage.getItem('vendorList'))

    switch (action.type) {
        case  "INSERT_VENDOR" :
            list.push(action.payload);
            localStorage.setItem('vendorList', JSON.stringify(list));
            return {list, currentIndex :state.currentIndex};
            
        case  "UPDATE_VENDOR" :
            list[state.currentIndex] = action.payload;
            localStorage.setItem('vendorList', JSON.stringify(list));
            return {list, currentIndex : state.currentIndex};
        
                  
        case  "DELETE_VENDOR" :
            list.splice(action.payload, 1);
            localStorage.setItem('vendorList', JSON.stringify(list));
            return {list, currentIndex : -1};
           
        default:
            return state;
    }
} 