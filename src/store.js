import { createStore, applyMiddleware, combineReducers, compose } from 'redux';

import { reducers } from './reducers';

if(localStorage.getItem('criteriaList') === null) {
    localStorage.setItem('criteriaList',JSON.stringify([]));
}

if(localStorage.getItem('vendorList') === null) {
    localStorage.setItem('vendorList',JSON.stringify([]));
}

const criteriaList = {
    list : JSON.parse(localStorage.getItem('criteriaList')),
    currentIndex: -1
}

const vendorList = {
    list : JSON.parse(localStorage.getItem('vendorList')),
    currentIndex: -1
}

const initialState = {
    criteriaList,
    vendorList
}
const combinedReducer = combineReducers(Object.assign({},reducers));

export const store = createStore( combinedReducer, initialState);