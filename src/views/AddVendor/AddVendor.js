import React, { Component } from 'react'
import { connect } from 'react-redux'
import { message, Modal } from 'antd'
import { VendorForm } from './VendorForm'
import { PlusCircleFilled } from '@ant-design/icons';
import Avatar from 'antd/lib/avatar/avatar';

 class AddVendor extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isModalVisible: false
        }
        this.showModal = this.showModal.bind(this);
    }

    showModal = () => {
        const { isModalVisible } = this.state
        if (this.props.list.length >= 4 && isModalVisible === false) {
            message.error('Maximum 4 vendors allowed');
        } else {
            this.setState({ isModalVisible: !isModalVisible })
        }
    };

    handleCancel = () => {
        this.showModal(false);
    };

    render() {
        const { isModalVisible } = this.state
        return (
            <div>

                <a onClick={this.showModal} >

                    {/* <PlusCircleFilled /> */}
                    <Avatar size={70} shape="square"  icon={<PlusCircleFilled style={{color: "green"}} />} />
                    <br />
                    <div style={{ marginTop: "10px" }}>
                        Add vendor
                        </div>
                </a>
                <Modal width={600} footer={null} title="Add vendor " size="large" visible={isModalVisible} onOk={this.handleOk} onCancel={this.handleCancel}>
                    <VendorForm handleCancel={this.handleCancel} />
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const { vendorList } = state
    return {
        list: vendorList.list
    }
}

const connected = connect(mapStateToProps)(
    AddVendor,
);

export { connected as AddVendor };
