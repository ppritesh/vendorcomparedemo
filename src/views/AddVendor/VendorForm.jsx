import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Input, Form, Button, message, InputNumber } from 'antd'
import { vendorActions } from '../../actions';
const { uuid } = require('uuidv4');

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 18 },
};

const tailLayout = {
    wrapperCol: { offset: 8, span: 18 },
};

class VendorForm extends Component {

    constructor(props) {
        super(props)
        this.formRef = React.createRef();
    }


    onFinish = (values) => {
        values.id = uuid();
        values.image = "https://picsum.photos/200?_id=" + values.id;
        if (this.props.list.length < 4) {
            this.props.dispatch(vendorActions.insert(values))
            this.props.handleCancel();
            this.formRef.current.resetFields();
            message.success('Vendor added succesfully.');
        } else {
            message.error('Max 4 vendors allowed');
        }
    };

    onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };


    render() {
        const checkRating = (_, value) => {
            console.log(value)
            if (value <= 0) {
                return Promise.reject('Value must be greater than 0.');
            } else if (value > 10) {
                return Promise.reject('Value must be less than 10.');
            }

            return Promise.resolve();
        };

        const { criteriaList } = this.props;

        return (
            <div>
                <Form  {...layout}
                    ref={this.formRef}
                    name="basic"
                    initialValues={{ remember: true }}
                    onFinish={this.onFinish}
                    onFinishFailed={this.onFinishFailed}>

                    <Form.Item
                        label="Vendor Name"
                        name="vendor_name"
                        rules={[{ required: true, message: 'Please enter vendor name' }]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Vendor Overall Score"
                        name="vendor_overall_score"

                        rules={[{ required: true, message: 'Please enter overall score' }, { validator: checkRating }]}
                    >
                        <InputNumber maxLength={3}
                            min={0}
                            mzx={10} />
                    </Form.Item>

                    {criteriaList.map((item) => {
                        if (item.type === 'collapsable') {
                            return <React.Fragment>
                                <Form.Item
                                    label={item.title}
                                    name={[item.id, 'value']}
                                    fieldKey={[item.id, 'value']}
                                    rules={[{ required: true, message: 'Please enter ' + item.title.toLowerCase() }]}
                                >
                                    <Input />
                                </Form.Item>
                                <div style={{ paddingLeft: "100px" }}>
                                    {item.children.map((childitem) => {
                                        return <Form.Item
                                            label={ childitem.title}
                                            name={[childitem.id, 'value']}
                                            fieldKey={[childitem.id, 'value']}
                                            rules={[{ required: true, message: 'Please enter ' + childitem.title.toLowerCase() }]}
                                        >
                                            <Input />
                                        </Form.Item>
                                    })}

                                </div>
                            </React.Fragment>
                        }

                        return <Form.Item
                            label={item.title}
                            name={[item.id, 'value']}
                            fieldKey={[item.id, 'value']}
                            rules={[{ required: true, message: 'Please enter ' + item.title.toLowerCase() }]}
                        >
                            <Input />
                        </Form.Item>
                    })}


                    <Form.Item {...tailLayout}>
                        <Button type="primary" htmlType="submit"> Submit  </Button>
                    </Form.Item>

                </Form>

            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const { vendorList, criteriaList } = state
    return {
        list: vendorList.list,
        criteriaList: criteriaList.list
    }
}

const connected = connect(mapStateToProps)(
    VendorForm,
);
export { connected as VendorForm };