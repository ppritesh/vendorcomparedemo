import { VendorsList } from "./VendorsList/VendorsList";
import 'antd/dist/antd.css'
import './app.css'
import { Card } from "antd";
import { Criteria } from "./Criteria/Criteria";


function App() {
  return (
    <Card title={<Criteria/>} style={{margin:"2em 7em"}}>
      <VendorsList />
    </Card>
  );
}

export default App;
