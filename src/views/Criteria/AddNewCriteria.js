import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Button, Modal } from 'antd';
import { CriteriaForm } from './CriteriaForm';
import { PlusOutlined } from '@ant-design/icons'

export class AddNewCriteria extends Component {
    static propTypes = {
        prop: PropTypes
    }

    constructor(props) {
        super(props);
        this.state = {

        }
        this.showModal = this.showModal.bind(this);
        this.handleOk = this.handleOk.bind(this);
    }

    showModal = () => {
        const { isModalVisible } = this.state
        this.setState({ isModalVisible: !isModalVisible })
    };

    handleOk = () => {
        this.showModal(false);
    };

    handleCancel = () => {
        this.showModal(false);
    };

    render() {
        const { isModalVisible } = this.state
        const { isMenu, menuData } = this.props

        return (
            <div>
                <Button type="primary"  onClick={this.showModal}>
                    {isMenu ? <PlusOutlined /> : "Add New Item"}
                </Button>
                <Modal title={isMenu && menuData ? menuData.title + " > Add new item" :  "Add new item"  }  footer={null} visible={isModalVisible} onOk={this.handleOk} onCancel={this.handleCancel}>

                    <CriteriaForm {...this.props} hideModal={this.showModal} />

                </Modal>


            </div>
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(AddNewCriteria)
