import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Button, Modal } from 'antd';
import { AddNewCriteria } from './AddNewCriteria';
import { CriteriaList } from './components/CriteriaList';

export class Criteria extends Component {
    static propTypes = {
        prop: PropTypes
    }

    constructor(props) {
        super(props);
        this.state = {

        }
        this.showModal = this.showModal.bind(this);
        this.handleOk = this.handleOk.bind(this);
    }

    showModal = () => {
        const { isModalVisible } = this.state
        this.setState({ isModalVisible: !isModalVisible })
    };

    handleOk = () => {
        this.showModal(false);
    };

    handleCancel = () => {
        this.showModal(false);
    };

    render() {
        const { isModalVisible } = this.state

        return (
            <div>
                <Button type="link" onClick={this.showModal}>
                    Add criteria
                </Button>
                <Modal width={600} title="criteria" footer={null} visible={isModalVisible} onOk={this.handleOk} onCancel={this.handleCancel}>
                    <AddNewCriteria />
                    
                    <CriteriaList />
                </Modal>


            </div>
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(Criteria)
