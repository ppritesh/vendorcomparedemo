import { Input, Select, Form, Button } from 'antd';
import React, { Component } from 'react';

import { connect } from 'react-redux'
import { criteriaActions } from '../../actions/criteria.actions';

const { uuid } = require('uuidv4');

const layout = {
    labelCol: { span: 3 },
    wrapperCol: { span: 21 },
};

const tailLayout = {
    wrapperCol: { offset: 3, span: 18 },
};

const INPUT_TYPES = {
    TEXT: 'text',
    SUB_ITEM: 'sub_item',
}



class CriteriaForm extends Component {
    constructor(props) {
        super(props);

        this.formRef = React.createRef();
        this.onFinish = this.onFinish.bind(this)
    }

    onFinish = (values) => {
        values.id = uuid();
        if(this.props.isMenu) {
            const {menuData, menuIndex} = this.props
            
            if(menuData.hasOwnProperty('children')) {
                menuData.children.push(values);
            }   else {
                menuData.children = [];
                menuData.children.push(values);
            }

            this.props.dispatch(criteriaActions.update(menuIndex, menuData ))

        }   else {
            this.props.dispatch(criteriaActions.insert(values))
        }
        this.props.hideModal();
        this.formRef.current.resetFields();
    };

    onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };


    render() {
        return (
            <Form  {...layout}
                ref={this.formRef}
                name="basic"
                initialValues={{ remember: true }}
                onFinish={this.onFinish}
                onFinishFailed={this.onFinishFailed}>

                <Form.Item
                    label="Title"
                    name="title"
                    rules={[{ required: true, message: 'Please input title' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item name="type" label="type" rules={[{ required: true, message: 'Please select type' }]}>
                    <Select
                        placeholder="Select type"
                        onChange={this.onGenderChange}
                        allowClear
                    >
                        <option value={'text'} >Text</option>
                        {!this.props.isMenu && 
                        <option value={'collapsable'}>Collapsable menu</option>}
                    </Select>
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit"> Submit  </Button>
                </Form.Item>

            </Form>
        );
    }
}

const mapStateToProps = (state) => {
    return state
}


const connected = connect(mapStateToProps)(
    CriteriaForm,
);
export { connected as CriteriaForm };