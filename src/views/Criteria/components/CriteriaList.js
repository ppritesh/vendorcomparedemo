import { Badge, Button, Col, List, Popconfirm, Popover, Row } from 'antd'
import { FormOutlined, DeleteOutlined } from '@ant-design/icons'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { criteriaActions } from '../../../actions'
import { AddNewCriteria } from '../AddNewCriteria'

class CriteriaList extends Component {

    handleDelete(index) {
        this.props.dispatch(criteriaActions.deleteItem(index))
    }

    handleSubItemDelete(parentIndex, indexToDelete, parentData) {
        parentData.children.splice(indexToDelete, 1);
        this.props.dispatch(criteriaActions.update(parentIndex, parentData))
    }


    render() {
        return (
            <div>
                <List
                    bordered
                    itemLayout="horizontal"
                    dataSource={this.props.list}
                    renderItem={(item, index) => (
                        <List.Item>
                            <Col span={14}> {item.title} &nbsp; &nbsp; <Badge
                                className="site-badge-count-109"
                                count={item.type}
                                style={{ backgroundColor: '#52c41a' }}
                            />
                            &nbsp; &nbsp;
                            <Popover content={<List
                                    bordered
                                    itemLayout="horizontal"
                                    dataSource={item.children || []}
                                    renderItem={(childitem, childIndex) => (
                                        <List.Item> {childitem.title}  &nbsp; &nbsp;
                                            <Popconfirm
                                                placement="topRight"
                                                title="Are you sure to delete this menu item?"
                                                onConfirm={() => this.handleSubItemDelete(index, childIndex, item)}
                                                // onCancel={cancel}
                                                okText="Yes"
                                                cancelText="No"
                                            >

                                                <Button type="danger" ><DeleteOutlined /></Button></Popconfirm></List.Item>)} />
                                } title="Menu Items">
                                    <Badge count={item.children ? item.children.length : 0} />
                                </Popover>

                            </Col>
                            <Col span={8}>
                                {item.type === "collapsable" &&
                                    <div style={{ float: "right" }}>

                                        <AddNewCriteria isMenu={true} menuIndex={index} menuData={item} />
                                    </div>
                                }

                                <Popconfirm
                                    placement="topRight"
                                    title="Are you sure to delete this criteria?"
                                    onConfirm={() => this.handleDelete(index)}
                                    // onCancel={cancel}
                                    okText="Yes"
                                    cancelText="No"
                                >

                                    <Button type="danger" style={{ float: "right" }}><DeleteOutlined /></Button></Popconfirm>
                            </Col>
                        </List.Item>
                    )}
                />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const { criteriaList } = state
    return {
        list: criteriaList.list
    }
}


const connected = connect(mapStateToProps)(
    CriteriaList,
);
export { connected as CriteriaList };