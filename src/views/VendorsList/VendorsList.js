import { CloseCircleOutlined, CloseOutlined, CaretRightOutlined, CaretDownOutlined } from '@ant-design/icons';
import { Avatar, Popconfirm, Progress } from 'antd';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { criteriaActions, vendorActions } from '../../actions';
import { AddVendor } from '../AddVendor/AddVendor';


import { Collapse } from 'react-collapse';
class VendorsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            collapsed: [],
        }
    }

    deleteVendor(index) {
        this.props.dispatch(vendorActions.deleteItem(index));
    }

    handleDelete(index) {
        this.props.dispatch(criteriaActions.deleteItem(index))
    }


    handleSubItemDelete(parentIndex, indexToDelete, parentData) {
        parentData.children.splice(indexToDelete, 1);
        this.props.dispatch(criteriaActions.update(parentIndex, parentData))
    }
    render() {

        const { collapsed } = this.state

        return (
            <div>

                <div className="ant-table">
                    <div className="ant-table-content">
                        <table style={{ tableLayout: 'auto', textAlign: 'center', verticalAlign: 'middle' }} border="1">
                            <tr className="ant-table-row ant-table-row-level-0">
                                <td className="ant-table-cell" style={{ width: "300px" }}><div> <AddVendor /> </div></td>
                                {this.props.list.length ? this.props.list.map((eachItem, index) => {
                                    return <td className="ant-table-cell" style={{ verticalAlign: 'middle' }}>
                                        <div style={{ position: "relative" }} >
                                            <Avatar size={80} key={index} src={eachItem.image || "https://picsum.photos/200?_id=" + eachItem.id} /> <br /> <div style={{ marginTop: "10px" }} >
                                                {eachItem.vendor_name}
                                                <div style={{ position: "absolute", top: 0, right: 0 }}>
                                                    <Popconfirm
                                                        placement="topRight"
                                                        title="Are you sure to delete this vendor?"
                                                        onConfirm={() => this.deleteVendor(index)}
                                                        // onCancel={cancel}
                                                        okText="Yes"
                                                        cancelText="No"
                                                    >
                                                        <a href="#">
                                                            <CloseOutlined style={{ fontSize: "18px", color: 'red' }} />
                                                        </a>
                                                    </Popconfirm>
                                                </div>
                                            </div> </div>
                                    </td>
                                }) : <td className="ant-table-cell">
                                        No vendors available
                                </td>}
                            </tr>
                            <tr className="ant-table-row ant-table-row-level-0 ">
                                <td align="left" className="ant-table-cell" >Overall Score</td>
                                {this.props.list.map((eachitem) => {
                                    return <td className="ant-table-cell" >  <Progress type="circle" percent={eachitem.vendor_overall_score * 10} format={percent => eachitem.vendor_overall_score} width={40} /></td>
                                })}
                            </tr>

                            {this.props.criteriaList && this.props.criteriaList.map((eachcriteria, index) => {
                                return eachcriteria.type === "collapsable" ?

                                    <React.Fragment>

                                        <tr className="ant-table-row ant-table-row-level-0 ">
                                            <td align="left" className="ant-table-cell" onClick={() => {
                                                collapsed[eachcriteria.id] = collapsed[eachcriteria.id] === true ? false : true;
                                                this.setState({ collapsed });
                                            }}  > {collapsed[eachcriteria.id] === true ? <CaretDownOutlined /> : <CaretRightOutlined />}  &nbsp; {eachcriteria.title}</td>
                                            {this.props.list.map((eachitem) => {
                                                return <td className="ant-table-cell" >
                                                    {eachitem.hasOwnProperty(eachcriteria.id) ? eachitem[eachcriteria.id].value : "-"}</td>
                                            })}
                                        </tr>
                                        {eachcriteria.hasOwnProperty('children') && eachcriteria.children.map((childItem, childIndex) => {

                                            return <tr lassName="ant-table-row ant-table-row-level-0"  >
                                                <td align="left" className="ant-table-cell p-0 b-0">

                                                    <Collapse isOpened={this.state.collapsed[eachcriteria.id]} >
                                                        <div style={{ padding: "10px", paddingLeft: "40px" }}>
                                                            {childItem.title}
                                                            
                                            <div style={{ float: "right" }}>
                                                            <Popconfirm
                                                                placement="topRight"
                                                                title="Are you sure to delete this menu item?"
                                                                onConfirm={() => this.handleSubItemDelete(index, childIndex, eachcriteria)}
                                                                // onCancel={cancel}
                                                                okText="Yes"
                                                                cancelText="No"
                                                            >
                                                                <CloseCircleOutlined style={{ color: 'red' }} /> </Popconfirm></div>
                                                        </div>
                                                    </Collapse>
                                                </td>
                                                {this.props.list.map((eachitem) => {
                                                    return <td className="ant-table-cell p-0 b-0">
                                                        <Collapse isOpened={this.state.collapsed[eachcriteria.id]} >
                                                            <div style={{ padding: "10px" }}>
                                                                {eachitem.hasOwnProperty(childItem.id) ? eachitem[childItem.id].value : "-"}
                                                            </div>
                                                        </Collapse>
                                                    </td>
                                                })}
                                            </tr>
                                        })}
                                    </React.Fragment>

                                    :
                                    <tr className="ant-table-row ant-table-row-level-0 ">
                                        <td align="left" className="ant-table-cell" >{eachcriteria.title}

                                            <div style={{ float: "right" }}>
                                                <Popconfirm
                                                    placement="topRight"
                                                    title="Are you sure to delete this criteria?"
                                                    onConfirm={() => this.handleDelete(index)}
                                                    // onCancel={cancel}
                                                    okText="Yes"
                                                    cancelText="No"
                                                >
                                                    <CloseCircleOutlined style={{ color: 'red' }} /></Popconfirm>

                                            </div>
                                        </td>

                                        {
                                            this.props.list.map((eachitem) => {

                                                return <td className="ant-table-cell" >

                                                    {eachitem.hasOwnProperty(eachcriteria.id) ? eachitem[eachcriteria.id].value : "-"}
                                                </td>

                                            })
                                        }


                                    </tr>
                            })}


                        </table>
                    </div>
                </div>
            </div >
        )
    }
}
const mapStateToProps = (state) => {
    const { vendorList, criteriaList } = state
    return {
        list: vendorList.list,
        criteriaList: criteriaList.list
    }
}


const connected = connect(mapStateToProps)(
    VendorsList,
);
export { connected as VendorsList };

